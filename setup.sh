#!/bin/sh
echo "Installing Xcode command-line tools..."
xcode-select --install
echo "Installing Homebrew package manager..."
echo "export HOMEBREW_PREFIX=~/usr/local" >> ~/.bash_profile
echo "export PATH=~/usr/local/bin:$PATH" >> ~/.bash_profile
echo "export EDITOR=/usr/bin/nano" >> ~/.bash_profile
mkdir ~/usr
mkdir ~/usr/local
curl https://gitlab.com/codekhs/setup-script/raw/master/brewinstall.rb | ruby
source ~/.bash_profile
echo "Installing Git and other essentials..."
brew doctor
brew install git git-lfs python3 go
#echo "Installing latest Unity..."
#brew cask install --appdir="~/Applications" unity
#echo "Done."
exit
