# KHS Coding Setup Script

This repository contains a script you can run to setup a development environment without administrator privileges on a KHS-issued MacBook
Air.

Copy and paste the following into a Terminal window, and press enter:

`curl https://gitlab.com/codekhs/setup-script/raw/master/setup.sh | sh`

This will download the script and run it in the `sh` interpreter.
